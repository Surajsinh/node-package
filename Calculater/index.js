const sum = (x, y) => x + y;
const min = (x, y) => x - y;
const mul = (x, y) => x * y;
const div = (x, y) => x / y;

module.exports = {
    sum: sum,
    min: min,
    mul: mul,
    div: div,
};
